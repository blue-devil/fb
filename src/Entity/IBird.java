package Entity;

public interface IBird
{
    abstract void flap();

    abstract double getX();

    abstract double getY();
}
